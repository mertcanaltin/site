import React from 'react';
import Head from 'next/head';

import {
    Navbar, Footer,
} from '../components';

export default () => (
  <div>
    <Head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
     </link>
      <title>Hakkımızda</title>
      
    </Head>
    <Navbar />
   <div className="jumbotron">
  <h1 className="display-4">Hakkımızda</h1>
  <p className="lead">Öncelikle bu siteden kendi adımıza hiçbir kazanç sağlamadığımızı belirtmek isteriz. Burada başarmak istediğimiz, yardıma muhtaç hayvanların hak ettikleri şekilde yaşamalarına destek olmaktır. Kapımızın önünden bir kap suyu, varsa artık yemeğimizi onlardan esirgememeliyiz. Birlikte yaşamayı öğrenmeli, sevsek de sevmesek de onları kabullenmeli, koruyup kollamalıyız.
Mutlu Barınak sisteminin amacı bir nebze de olsa bunu vaadediyor.
Bir iban üzerinden para bağışı şeklinde değil de daha güvenilir bir sistemle ne aldığınızı bilerek ve görerek bağış yapabilirsiniz.

Peki, bu bağışlar nereye gidecek?
Tabiki de desteğe muhtaç olan barınaklarımıza.

Çeşitli illerdeki çeşitli barınakların üye olduğu bu sistemde barınağın neye ihtiyacı olduğu, barınak adı/bulunduğu il şeklindeki başlığın altında listelenecek ve siz, bu ihtiyaç listesinden arzuladığınız öğeyi bağış sistemimiz altında alıp bizim aracılığımızla barınağa bağışlayabileceksiniz. 
Bağışınız, satın almadan tut teslime kadar her adımında size mail yoluyla bildirilecek ve gerek fotoğraflarla gerek de barınak yönetimindeki kişinin dönütüyle desteklenecek. Sitemizde de yayınlanacak. 

Ek olarak, belirlenen günlerde evcil havvanları olanlar için, onların ve dostlarının güzel vakit geçirmesi adına çeşitli buluşmalar, sokak hayvanlarına mama dağıtımı ve sponsor eşliğinde temin edilen malzemelerle kedi/köpek/kuş kulübeleri yapma etkinlikleri düzenlenecektir.
Kaybolan dostlarımızı en hızlı şekilde yuvalarına kavuşturmak için “Kayıp İlanları” ve “Sokağa atma sahiplen/sahiplendir” başlıkları altında ilanlarınızı bu sitede yayınlayabileceksiniz. Bu yolla daha fazla kitleye ulaşabilirsiniz.

Biz, hayvanseverler için bu sistemi olabildiğince güvenli ve açık bir şekilde hayata geçirme niyetindeyiz. Eğer bu niyetimiz gerçekleşir ve birlikte yaşamaya bir adım daha yaklaşabilirsek ne mutlu bize!
Her türlü yardımlarınız ve güzel kalbiniz için teşekkürler.

Sevgilerle,
Mutlu Barınak</p>
  <hr className="my-4"/>
  <p></p>
  <p className="lead">
    <a className="btn btn-primary btn-lg" href="#" role="button">Hemen Bağış Yap</a>
  </p>
</div>
    
    <Footer />
  </div>
);

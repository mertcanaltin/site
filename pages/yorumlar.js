import React from 'react';
import Head from 'next/head';

import {
   Quotes, Navbar, Footer,
} from '../components';

export default () => (
  <div>
    <Head>
      <title>Yorumlar </title>
    </Head>
   
    < Navbar/>
   <Quotes/>
    <Footer />
  </div>
);

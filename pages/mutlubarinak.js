import React from 'react';
import Head from 'next/head';


import {
    Navbar, Footer,
} from '../components';

export default () => (
  <div>
    <Head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
     </link>
      <title>Mutlu Barınak Nedir?</title>
      
    </Head>
    <Navbar />
  <main role="main" className="container">
  <style jsx>
 {`
        .starter-template {
  padding: 3rem 1.5rem;
  text-align: center;
}
      `}
  </style>
        <div className="starter-template">
           <img src="./assets/catdog.gif" width={300} alt="" style={{display: 'inline'}} />
          <h1>Musmutlu Bir Barınak !</h1>
          <p className="lead">Her türlü yardımlarınız ve güzel kalbiniz için teşekkürler.</p>
        </div>
      </main>
    <Footer />
  </div>
);

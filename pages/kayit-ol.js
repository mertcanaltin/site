import React from 'react';
import Head from 'next/head';


import {
    Navbar, Footer,
} from '../components';

export default () => (
  <div>
    <Head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
     </link>
      <title>Kayıt ol</title>
      
    </Head>
    <Navbar />
  <style jsx>
 {`
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
      html {
  font-size: 14px;
}
@media (min-width: 768px) {
  html {
    font-size: 16px;
  }
}

.container {
  max-width: 960px;
}

.pricing-header {
  max-width: 700px;
}

.card-deck .card {
  min-width: 220px;
}
      `}
  </style>
       
 <div>
        <div className="pricing-header px-3 py-3 pt-md-5 pb-md-4 mx-auto text-center">
          <h1 className="display-4">Ücretsiz gönüllü ol!</h1>
          <p className="lead">Mutlu Barınak ailesine hoşgeldin !</p>
          <p className="lead">işte bu mükemmel bir adım </p>
          <p className="lead">patiler buna çok sevindi!</p>
            <img src="./assets/catsleep.gif" width={200} alt="" style={{display: 'inline'}} />
        </div>
        <div className="container">
          <div className="card-deck mb-3 text-center">
            <div className="card mb-4 shadow-sm">
              <div className="card-header">
                <h4 className="my-0 font-weight-normal">Ben Gönüllüyüm</h4>
              </div>
              <div className="card-body">
                <ul className="list-unstyled mt-3 mb-4">
                  <li>*Patilerle arası çok iyi olan</li>
                  <li>*bir bağışcı adayı </li>
                </ul>
                <button type="button" className="btn btn-lg btn-block btn-primary">Gönüllü olarak kayıt ol</button>
              </div>
            </div>
            <div className="card mb-4 shadow-sm">
              <div className="card-header">
                <h4 className="my-0 font-weight-normal">Ben Barınak Yöneticisiyim</h4>
              </div>
              <div className="card-body">
                <ul className="list-unstyled mt-3 mb-4">
                  <li>*Belediye onaylı bir evrak </li>
                  <li>*Barınak fotoğrafları ve detayları</li>
                  <li>*Barınak adres bilgileri</li>
                   <li>*Süper bağışcılar ile ilgilenen</li>
                </ul>
                <button type="button" className="btn btn-lg btn-block btn-primary">Barınak olarak kayıt ol</button>
              </div>
            </div>
            <div className="card mb-4 shadow-sm">
              <div className="card-header">
                <h4 className="my-0 font-weight-normal">Sahiplen / Sahiplendir </h4>
              </div>
              <div className="card-body">
                <h1 className="card-title pricing-card-title">Çok <small className="text-muted">Yakında</small></h1>
                <ul className="list-unstyled mt-3 mb-4">
                  <li></li>
                  
                </ul>
                
              </div>
            </div>
          </div>
        </div></div>


    <Footer />
  </div>
);

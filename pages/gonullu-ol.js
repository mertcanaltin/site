import React from 'react';
import Head from 'next/head';


import {
    Navbar, Footer,
} from '../components';

export default () => (
  <div>
    <Head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
     </link>
      <title>Gönüllü ol !</title>
      
    </Head>
    <Navbar />
  <style jsx>
 {`
     .container {
  max-width: 960px;
}

.lh-condensed { line-height: 1.25; }
.center_div{
    margin: 0 auto;
    width:80% /* value of your choice which suits your alignment */
}
      `}
  </style>
          <div className="container center_div">
        <div className="py-5 text-center">
         <img src="./assets/superheros.gif" width={200} alt="" style={{display: 'inline'}} />
          <h2>Bunu duyduğumuza çok sevindik !</h2>
          <p className="lead">Hey! artık bir gönüllüsün patilerin gücü adına!</p>
        </div>
        <div className="row align-self-center">
          <div className="col-md-4 order-md-2 mb-4">
          </div>
          <div className="col-md-8 order-md-1">
            <h4 className="mb-3">Bilgilerin</h4>
            <form className="needs-validation center_div" noValidate>
              <div className="row">
                <div className="col-md-6 mb-3">
                  <label htmlFor="firstName">Adınız</label>
                  <input type="text" className="form-control" id="firstName" placeholder defaultValue required />
                  <div className="invalid-feedback">
                    Valid first name is required.
                  </div>
                </div>
                <div className="col-md-6 mb-3">
                  <label htmlFor="lastName">Soyadınız</label>
                  <input type="text" className="form-control" id="lastName" placeholder defaultValue required />
                  <div className="invalid-feedback">
                    Valid last name is required.
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <label htmlFor="username">Kullanıcı adınız</label>
                <div className="input-group">
                  <div className="input-group-prepend">
                    <span className="input-group-text">Süper biri = </span>
                  </div>
                  <input type="text" className="form-control" id="username" placeholder="Kullanıcı Adınız" required />
                  <div className="invalid-feedback" style={{width: '100%'}}>
                    Your username is required.
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <label htmlFor="username">Şifreniz</label>
                <div className="input-group">
                  <div className="input-group-prepend">
                  </div>
                  <input type="password" className="form-control" id="password" placeholder="Şifreniz" required />
                  <div className="invalid-feedback" style={{width: '100%'}}>
                    Your username is required.
                  </div>
                </div>
              </div>
              <div className="mb-3">
                <label htmlFor="email">Email <span className="text-muted" /></label>
                <input type="email" className="form-control" id="email" placeholder="seninepostan@example.com" />
                <div className="invalid-feedback">
                  Please enter a valid email address for shipping updates.
                </div>
              </div>
              <div className="mb-3">
                <label htmlFor="address">Adres</label>
                <input type="text" className="form-control" id="address" placeholder="Mutlu Barınak izmir" required />
                <div className="invalid-feedback">
                  Please enter your shipping address.
                </div>
              </div>
              <div className="row">
                <div className="col-md-5 mb-3">
                  <label htmlFor="country">Şehir Seçimi</label>
                  <select className="custom-select d-block w-100" id="country" required>
                    <option value>Şehir Seç...</option>
                    <option value={0}>------</option>
                    <option value={1}>Adana</option>
                    <option value={2}>Adıyaman</option>
                    <option value={3}>Afyonkarahisar</option>
                    <option value={4}>Ağrı</option>
                    <option value={5}>Amasya</option>
                    <option value={6}>Ankara</option>
                    <option value={7}>Antalya</option>
                    <option value={8}>Artvin</option>
                    <option value={9}>Aydın</option>
                    <option value={10}>Balıkesir</option>
                    <option value={11}>Bilecik</option>
                    <option value={12}>Bingöl</option>
                    <option value={13}>Bitlis</option>
                    <option value={14}>Bolu</option>
                    <option value={15}>Burdur</option>
                    <option value={16}>Bursa</option>
                    <option value={17}>Çanakkale</option>
                    <option value={18}>Çankırı</option>
                    <option value={19}>Çorum</option>
                    <option value={20}>Denizli</option>
                    <option value={21}>Diyarbakır</option>
                    <option value={22}>Edirne</option>
                    <option value={23}>Elazığ</option>
                    <option value={24}>Erzincan</option>
                    <option value={25}>Erzurum</option>
                    <option value={26}>Eskişehir</option>
                    <option value={27}>Gaziantep</option>
                    <option value={28}>Giresun</option>
                    <option value={29}>Gümüşhane</option>
                    <option value={30}>Hakkâri</option>
                    <option value={31}>Hatay</option>
                    <option value={32}>Isparta</option>
                    <option value={33}>Mersin</option>
                    <option value={34}>İstanbul</option>
                    <option value={35}>İzmir</option>
                    <option value={36}>Kars</option>
                    <option value={37}>Kastamonu</option>
                    <option value={38}>Kayseri</option>
                    <option value={39}>Kırklareli</option>
                    <option value={40}>Kırşehir</option>
                    <option value={41}>Kocaeli</option>
                    <option value={42}>Konya</option>
                    <option value={43}>Kütahya</option>
                    <option value={44}>Malatya</option>
                    <option value={45}>Manisa</option>
                    <option value={46}>Kahramanmaraş</option>
                    <option value={47}>Mardin</option>
                    <option value={48}>Muğla</option>
                    <option value={49}>Muş</option>
                    <option value={50}>Nevşehir</option>
                    <option value={51}>Niğde</option>
                    <option value={52}>Ordu</option>
                    <option value={53}>Rize</option>
                    <option value={54}>Sakarya</option>
                    <option value={55}>Samsun</option>
                    <option value={56}>Siirt</option>
                    <option value={57}>Sinop</option>
                    <option value={58}>Sivas</option>
                    <option value={59}>Tekirdağ</option>
                    <option value={60}>Tokat</option>
                    <option value={61}>Trabzon</option>
                    <option value={62}>Tunceli</option>
                    <option value={63}>Şanlıurfa</option>
                    <option value={64}>Uşak</option>
                    <option value={65}>Van</option>
                    <option value={66}>Yozgat</option>
                    <option value={67}>Zonguldak</option>
                    <option value={68}>Aksaray</option>
                    <option value={69}>Bayburt</option>
                    <option value={70}>Karaman</option>
                    <option value={71}>Kırıkkale</option>
                    <option value={72}>Batman</option>
                    <option value={73}>Şırnak</option>
                    <option value={74}>Bartın</option>
                    <option value={75}>Ardahan</option>
                    <option value={76}>Iğdır</option>
                    <option value={77}>Yalova</option>
                    <option value={78}>Karabük</option>
                    <option value={79}>Kilis</option>
                    <option value={80}>Osmaniye</option>
                    <option value={81}>Düzce</option>
                  </select>
                  <div className="invalid-feedback">
                    Please select a valid country.
                  </div>
                </div>
              </div>
            </form></div>
        </div>
        <hr className="mb-4" />
        <h3>Mutlu Barınak Kâr amacı gütmeyen bir kuruluştur.</h3>
        <button className="btn btn-primary btn-lg btn-block" type="submit">Üye ol</button>
      </div>

    <Footer />
  </div>
);
